# C-Tetris
A simple Tetris-like game written in C using SDL2.

## Table of contents
* [Dependencies](#dependencies)
* [Setup](#setup)
	
## Dependencies
Program requires following libraries to be installed:

* SDL2-devel (providing SDL2/SDL.h)
* SDL2_gfx-devel (providing SDL2/SDL2_gfxPrimitives.h)
	
## Setup
To compile this project use Make:
```
$ make
```

Run the compiled program:
```
$ ./main
```

To remove files after compilation:
```
$ make clean
```
