// Created by Maciej Stępień

#include "primlib.h"
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>

#define BACKGROUND_COLOR BLACK
#define BRICK_COLOR WHITE
#define ACTOR_COLOR YELLOW
#define SCORE_COLOR YELLOW
#define GUI_COLOR WHITE

#define ROWS 20
#define COLS 10
#define BRICK_SIZE 20
#define BRICK_SPACING 4
#define BLOCKS_QUEUE 3
#define BLOCK_SIZE 4
#define BLOCK_TYPES 7
#define WINDOW_BORDER 10

#define DELAY_TIME 10
#define DOWN_STEP 5
#define DROP_STEP 2

#define DOCK_SCORE BLOCK_SIZE
#define LINE_COMPLETE_SCORE COLS

typedef struct {
    int x;
    int y;
    int width;
    int height;
    enum color color;
} Rectangle;


typedef struct {
    int x;
    int y;
    int width;
    int height;
    int border;
    enum color color;
} BorderedRectangle;


typedef struct {
    int rotationNumber;
    bool (*bricks)[BLOCK_SIZE][BLOCK_SIZE];
} Block;


typedef struct {
    int column;
    int row;
    Block block;
} Actor;


void calcLeftRotatedBrick(int brickCoordinates[2], int blockWidth, int totalWidth)
{
    int tempY = brickCoordinates[1];
    brickCoordinates[1] = brickCoordinates[0];
    brickCoordinates[0] = tempY;

    brickCoordinates[0] = totalWidth - 1 - brickCoordinates[0];
    brickCoordinates[0] -= (totalWidth - blockWidth);
}


int getBricksWidth(int cols, int rows, bool bricks[cols][rows])
{
    for (int x = 0; x < cols; x++)
    {
        bool emptyCol = true;
        for (int y = 0; y < rows; y++)
        {
            if (bricks[x][y])
            {
                emptyCol = false;
                break;
            }
        }
        if (emptyCol)
            return x;
    }
    return cols;
}


int getBricksHeight(int cols, int rows, bool bricks[cols][rows])
{
    for (int y = 0; y < rows; y++)
    {
        bool emptyRow = true;
        for (int x = 0; x < cols; x++)
        {
            if (bricks[x][y])
            {
                emptyRow = false;
                break;
            }
        }
        if (emptyRow)
            return y;
    }
    return rows;
}


void calcRotatedBrick(int brickCoordinates[2], Block block)
{
    int bricksWidth = getBricksWidth(BLOCK_SIZE, BLOCK_SIZE, (*block.bricks));
    int bricksHeight = getBricksHeight(BLOCK_SIZE, BLOCK_SIZE, (*block.bricks));
    for (int rotationIndex = 0; rotationIndex < block.rotationNumber; rotationIndex++)
    {
        int tempWidth = bricksWidth;
        bricksWidth = bricksHeight;
        bricksHeight = tempWidth;
        calcLeftRotatedBrick(brickCoordinates, bricksWidth, BLOCK_SIZE);
    }
}


void rotate(int rotationNumber, Actor *actor)
{
    int actorRotations = (*actor).block.rotationNumber + rotationNumber;
    while (actorRotations < 0)
        actorRotations += 4;
    actorRotations %= 4;
    actor->block.rotationNumber = actorRotations;
}


void drawRectangle(int x, int y, int width, int height, int color)
{
    gfx_filledRect(x, y - height, x + width, y, color);
}


void drawRectangleStruct(Rectangle rectangle)
{
    drawRectangle(rectangle.x,
                  rectangle.y,
                  rectangle.width,
                  rectangle.height,
                  rectangle.color);
}


void drawBorderedRectangle(int x, int y, int width, int height, int border, enum color color)
{
    drawRectangle(x - border,
                  y + border,
                  width + border * 2,
                  height + border * 2,
                  color);
    drawRectangle(x, y, width, height, BACKGROUND_COLOR);
}


void drawBorderedRectangleStruct(BorderedRectangle borderedRectangle)
{
    drawRectangle(borderedRectangle.x - borderedRectangle.border,
                  borderedRectangle.y + borderedRectangle.border,
                  borderedRectangle.width + borderedRectangle.border * 2,
                  borderedRectangle.height + borderedRectangle.border * 2,
                  borderedRectangle.color);
    drawRectangle(borderedRectangle.x,
                  borderedRectangle.y,
                  borderedRectangle.width,
                  borderedRectangle.height,
                  BACKGROUND_COLOR);
}


Block createBlock(int rotationNumber, bool (*bricks)[BLOCK_SIZE][BLOCK_SIZE])
{
    Block block;
    block.rotationNumber = rotationNumber;
    block.bricks = bricks;
    return block;
}


Actor createActor(int column, int row, Block block)
{
    Actor actor;
    actor.column = column;
    actor.row = row;
    actor.block = block;
    return actor;
}


Block createRandomBlock()
{
    static bool templates[BLOCK_TYPES][BLOCK_SIZE][BLOCK_SIZE] = {
        {   {1,1,1,1},
            {0,0,0,0},
            {0,0,0,0},
            {0,0,0,0}
        },

        {   {1,1,1,0},
            {0,1,0,0},
            {0,0,0,0},
            {0,0,0,0}
        },

        {   {1,0,0,0},
            {1,1,0,0},
            {0,1,0,0},
            {0,0,0,0}
        },

        {   {0,1,0,0},
            {1,1,0,0},
            {1,0,0,0},
            {0,0,0,0},
        },

        {   {1,1,1,0},
            {0,0,1,0},
            {0,0,0,0},
            {0,0,0,0}
        },

        {   {1,1,1,0},
            {1,0,0,0},
            {0,0,0,0},
            {0,0,0,0}
        },

        {   {1,1,0,0},
            {1,1,0,0},
            {0,0,0,0},
            {0,0,0,0}
        }
    };

    int index = rand() % BLOCK_TYPES;
    int rotation = rand() % 4;
    Block block = createBlock(rotation, &(templates[index]));
    return block;
}


void setActorRandomValues(Actor *actor, Block block)
{
    int limit = 2;
    actor->column = rand() % (COLS - limit - 1);
    actor->row = ROWS - 1;
    actor->block = block;
}


Actor createRandomActor()
{
    Block block = createRandomBlock();
    int limit = 2;
    int column = rand() % (COLS - 2 * limit) + limit;
    Actor actor = createActor(column, ROWS - 1, block);

    return actor;
}


void exchangeActor(Actor *actor, Block (*blockQueue)[BLOCKS_QUEUE])
{
    setActorRandomValues(actor, (*blockQueue)[0]);
    for (int blockIndex = 0; blockIndex < BLOCKS_QUEUE - 1; blockIndex++)
        (*blockQueue)[blockIndex] = (*blockQueue)[blockIndex + 1];
    (*blockQueue)[BLOCKS_QUEUE - 1] = createRandomBlock();
}


void fill2DBool(bool val, int cols, int rows, bool bricks[cols][rows])
{
    for (int x = 0; x < COLS; x++)
        for (int y = 0; y < ROWS; y++)
            bricks[x][y] = val;
}


void drawBoard(int posX, int posY, bool bricks[COLS][ROWS])
{
    for (int x = 0; x < COLS; x++)
        for (int y = 0; y < ROWS; y++)
            if (bricks[x][y] == true)
            {
                drawRectangle(posX + (BRICK_SIZE + BRICK_SPACING) * x,
                              posY - (BRICK_SIZE + BRICK_SPACING) * y,
                              BRICK_SIZE, BRICK_SIZE, BRICK_COLOR);
            }
}


void drawBlock(int posX, int posY, Block block)
{
    int rotatedCoordinates[2] = {0};
    for (int x = 0; x < BLOCK_SIZE; x++)
        for (int y = 0; y < BLOCK_SIZE; y++)
        {
            if ((*block.bricks)[x][y] == true)
            {
                enum color brickColor = BRICK_COLOR;
                rotatedCoordinates[0] = x;
                rotatedCoordinates[1] = y;
                calcRotatedBrick(rotatedCoordinates, block);
                drawRectangle(posX + (BRICK_SIZE + BRICK_SPACING) * (rotatedCoordinates[0]) + BRICK_SPACING,
                              posY - (BRICK_SIZE + BRICK_SPACING) * (rotatedCoordinates[1]) - BRICK_SPACING,
                              BRICK_SIZE, BRICK_SIZE, brickColor);
            }
        }
}


void drawActor(int posX, int posY, Actor actor)
{
    int rotatedCoordinates[2] = {0};
    for (int x = 0; x < BLOCK_SIZE; x++)
        for (int y = 0; y < BLOCK_SIZE; y++)
        {
            if ((*actor.block.bricks)[x][y] == true)
            {
                rotatedCoordinates[0] = x;
                rotatedCoordinates[1] = y;
                calcRotatedBrick(rotatedCoordinates, actor.block);
                drawRectangle(posX + (BRICK_SIZE + BRICK_SPACING) * (rotatedCoordinates[0] + actor.column) + BRICK_SPACING,
                              posY - (BRICK_SIZE + BRICK_SPACING) * (rotatedCoordinates[1] + actor.row) - BRICK_SPACING,
                              BRICK_SIZE, BRICK_SIZE, ACTOR_COLOR);
            }
        }
}


void drawBlocksQueue(int x, int y, Block blockQueue[BLOCKS_QUEUE])
{
    y += BRICK_SPACING + (BRICK_SPACING + BRICK_SIZE) * BLOCK_SIZE;
    for (int blockIndex = 0; blockIndex < BLOCKS_QUEUE; blockIndex++)
    {
        drawBorderedRectangle(x,
                              y + (BRICK_SPACING * 2 + (BRICK_SPACING + BRICK_SIZE) * BLOCK_SIZE) * blockIndex,
                              BRICK_SPACING + (BRICK_SPACING + BRICK_SIZE) * BLOCK_SIZE,
                              BRICK_SPACING + (BRICK_SPACING + BRICK_SIZE) * BLOCK_SIZE,
                              BRICK_SPACING,
                              GUI_COLOR);
        drawBlock(x,
                  y + (BRICK_SPACING * 2 + (BRICK_SPACING + BRICK_SIZE) * BLOCK_SIZE) * blockIndex,
                  blockQueue[blockIndex]
                 );
    }
}


void drawScore(int x, int y, int score)
{
    char scoreText[10];
    SDL_itoa(score, scoreText, 10);
    gfx_textout(x, y, "SCORE:", GUI_COLOR);
    gfx_textout(x, y + 15, scoreText, SCORE_COLOR);
}


void drawGameOver(int x, int y)
{
    gfx_textout(x, y, "Game over!", GUI_COLOR);
    gfx_textout(x, y + 30, "Press ENTER to play again.", GUI_COLOR);
    gfx_textout(x, y + 45, "Press ESCAPE to exit the game.", GUI_COLOR);
}


void drawScene(int score, bool gameOver, Actor *actor, bool board[COLS][ROWS], Block blockQueue[BLOCK_SIZE])
{
    static bool firstRun = true;
    static BorderedRectangle gameBox;
    static Rectangle blackBox;

    if (firstRun)
    {
        gameBox.width = COLS * (BRICK_SIZE + BRICK_SPACING) + BRICK_SPACING;
        gameBox.height = ROWS * (BRICK_SIZE + BRICK_SPACING) + BRICK_SPACING;
        gameBox.x = WINDOW_BORDER;
        gameBox.y = WINDOW_BORDER + gameBox.height;
        gameBox.border = 4;
        gameBox.color = GUI_COLOR;

        blackBox.x = gameBox.x - gameBox.border;
        blackBox.y = gameBox.y - gameBox.height - gameBox.border;
        blackBox.width = gameBox.width + gameBox.border * 2;
        blackBox.height = 500;
        blackBox.color = BLACK;
        firstRun = false;
    }

    drawBorderedRectangleStruct(gameBox);
    drawBoard(gameBox.x + BRICK_SPACING, gameBox.y - BRICK_SPACING, board);
    drawActor(gameBox.x, gameBox.y, *actor);
    drawRectangleStruct(blackBox);
    drawBlocksQueue(gameBox.x + gameBox.width + gameBox.border + 10,
                    gameBox.y - gameBox.height,
                    blockQueue);
    drawScore(gameBox.x + gameBox.width + gameBox.border + 10,
              gameBox.y - gameBox.height + (BRICK_SPACING * 2 + (BRICK_SPACING + BRICK_SIZE) * BLOCK_SIZE) * BLOCKS_QUEUE + 30,
              score);
    if (gameOver)
        drawGameOver(gameBox.x, gameBox.y + 30);
}


void dockActor(Actor *actor, bool board[COLS][ROWS])
{
    int transformedCoordinates[2];
    for (int x = 0; x < BLOCK_SIZE; x++)
    {
        for (int y = 0; y < BLOCK_SIZE; y++)
        {
            if ((*(*actor).block.bricks)[x][y])
            {
                transformedCoordinates[0] = x;
                transformedCoordinates[1] = y;
                calcRotatedBrick(transformedCoordinates, (*actor).block);
                transformedCoordinates[0] += (*actor).column;
                transformedCoordinates[1] += (*actor).row;
                board[transformedCoordinates[0]][transformedCoordinates[1]] = 1;
            }
        }
    }
}


bool isActorColliding(bool checkTopBorder, Actor *actor, bool board[COLS][ROWS])
{
    int transformedCoordinates[2];
    for (int x = 0; x < BLOCK_SIZE; x++)
    {
        for (int y = 0; y < BLOCK_SIZE; y++)
        {
            if ((*(*actor).block.bricks)[x][y] == false)
            {
                continue;
            }
            transformedCoordinates[0] = x;
            transformedCoordinates[1] = y;
            calcRotatedBrick(transformedCoordinates, (*actor).block);
            transformedCoordinates[0] += (*actor).column;
            transformedCoordinates[1] += (*actor).row;

            if (transformedCoordinates[0] >= 0 && transformedCoordinates[0] < COLS
                    && transformedCoordinates[1] >= 0)
            {
                if (checkTopBorder && transformedCoordinates[1] >= ROWS)
                    return true;
                if (transformedCoordinates[1] < ROWS && board[transformedCoordinates[0]][transformedCoordinates[1]])
                    return true;
            }
            else
                return true;
        }
    }
    return false;
}


bool isGameOver(Actor *actor, bool board[COLS][ROWS])
{
    actor->row -= 1;
    if (isActorColliding(false, actor, board))
    {
        actor->row += 1;
        if (isActorColliding(true, actor, board))
        {
            return true;
        }
        actor->row -= 1;
    }
    actor->row += 1;
    return false;
}


void moveLeft(Actor *actor, bool board[COLS][ROWS])
{
    actor->column -= 1;
    if (isActorColliding(false, actor, board))
        actor->column += 1;
}


void moveRight(Actor *actor, bool board[COLS][ROWS])
{
    actor->column += 1;
    if (isActorColliding(false, actor, board))
        actor->column -= 1;
}


bool moveDown(int *score, bool *gameOver, Actor *actor, bool board[COLS][ROWS], Block (*blockQueue)[BLOCKS_QUEUE])
{
    if ((*gameOver))
        return false;

    actor->row -= 1;
    if (isActorColliding(false, actor, board))
    {
        actor->row += 1;
        dockActor(actor, board);
        exchangeActor(actor, blockQueue);
        (*score) += DOCK_SCORE;

        (*gameOver) = isGameOver(actor, board);
        return false;
    }
    return true;
}


void reinit(int *score, bool *gameOver, Actor *actor, bool (*board)[COLS][ROWS], Block (*blockQueue)[BLOCKS_QUEUE])
{
    (*score) = 0;
    (*gameOver) = false;
    exchangeActor(actor, blockQueue);
    for (int x = 0; x < COLS; x++)
        for (int y = 0; y < ROWS; y++)
            (*board)[x][y] = 0;
}


void handleInput(int *score, bool *gameOver, Actor *actor, bool (*board)[COLS][ROWS], Block (*blockQueue)[BLOCKS_QUEUE])
{
    int key = gfx_pollkey();
    switch(key)
    {
    case(SDLK_LEFT):
        if (!(*gameOver))
            moveLeft(actor, (*board));
        break;
    case(SDLK_RIGHT):
        if (!(*gameOver))
            moveRight(actor, (*board));
        break;
    case(SDLK_DOWN):
        if (!(*gameOver))
            moveDown(score, gameOver, actor, (*board), blockQueue);
        break;
    case(SDLK_SPACE):
        if (!(*gameOver))
        {
            rotate(-1, actor);
            if (isActorColliding(false, actor, (*board)))
                rotate(1, actor);
        }
        break;
    case(SDLK_ESCAPE):
        exit(0);
        break;
    case(SDLK_RETURN):
        reinit(score, gameOver, actor, board, blockQueue);
        break;
    }
}


void eraseRow(int row, bool board[COLS][ROWS])
{
    for (int y = row; y < ROWS - 1; y++)
    {
        for (int x = 0; x < COLS; x++)
        {
            board[x][y] = board[x][y + 1];
        }
    }
    for (int x = 0; x < COLS; x++)
    {
        board[x][ROWS - 1] = 0;
    }
}


void checkCompletedRows(int *score, bool board[COLS][ROWS])
{
    for (int y = 0; y < ROWS; y++)
    {
        bool completed = true;
        for (int x = 0; x < COLS; x++)
        {
            if (board[x][y] == false)
            {
                completed = false;
                break;
            }
        }
        if (completed) {
            eraseRow(y, board);
            (*score) += LINE_COMPLETE_SCORE;
        }
    }
}


void frequentMove(int *score, bool *gameOver, Actor *actor, bool board[COLS][ROWS], Block (*blockQueue)[BLOCKS_QUEUE])
{
    static bool firstRun = true;
    static Uint64 last;
    if (firstRun)
    {
        last = SDL_GetTicks();
        moveDown(score, gameOver, actor, board, blockQueue);
        firstRun = false;
        return;
    }
    Uint64 deltaTime = SDL_GetTicks() - last;
    if (deltaTime > 500)
    {
        moveDown(score, gameOver, actor, board, blockQueue);
        last = SDL_GetTicks();
    }
}


int main(int argc, char *argv[]) {
    if (gfx_init())
        exit(3);

    bool running = true;
    bool gameOver = false;
    bool board[COLS][ROWS];
    int score = 0;
    fill2DBool(false, COLS, ROWS, board);
    srand((unsigned) time(0));

    Actor actor = createRandomActor();
    Block blockQueue[BLOCKS_QUEUE];
    for (int blockIndex = 0; blockIndex < BLOCKS_QUEUE; blockIndex++)
        blockQueue[blockIndex] = createRandomBlock();

    while (running) {
        gfx_filledRect(0, 0, gfx_screenWidth() - 1, gfx_screenHeight() - 1, BACKGROUND_COLOR);
        handleInput(&score, &gameOver, &actor, &board, &blockQueue);

        if(!gameOver)
        {
            frequentMove(&score,&gameOver, &actor, board, &blockQueue);
            checkCompletedRows(&score, board);
            gameOver = isGameOver(&actor, board);
        }
        drawScene(score, gameOver, &actor, board, blockQueue);
        gfx_updateScreen();
        SDL_Delay(DELAY_TIME);
    }

    gfx_getkey();
    return 0;
}
